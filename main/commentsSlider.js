document.addEventListener('DOMContentLoaded', () => {
  new Glide('.glide-com', {
    autoplay: false,
    peek: 0,
    animationDuration: 1000,
    type: 'carousel',
    animationTimingFunc: 'ease',
    startAt: 0,
    focusAt: 0,
    perView: 1
  }).mount()
});