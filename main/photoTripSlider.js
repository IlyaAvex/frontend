document.addEventListener('DOMContentLoaded', () => {
  new Glide('.glide', {
    autoplay: 6000,
    peek: 400,
    animationDuration: 1000,
    type: 'carousel',
    animationTimingFunc: 'ease',
    startAt: 1,
    focusAt: 0,
    perView: 1,
    breakpoints: {
      1400: {
        peek: 300
      },
      1024: {
        peek: 200
      },
      990: {
        perView: 1,
        peek: {
          before: 0,
          after: 0
        }
      }
    }
  }).mount()
});