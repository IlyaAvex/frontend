document.addEventListener('DOMContentLoaded', () => {
  ymaps.ready(init);
  function init(){
    let myMap = new ymaps.Map("map", {
      center: [51.146991, 71.370340],
      zoom: 17,
      controls: []
    });

    let myPlacemark = new ymaps.Placemark([51.146891, 71.370520]);
    myMap.geoObjects.add(myPlacemark);
  }
})