document.addEventListener('DOMContentLoaded', () => {
  const
    body = document.querySelector('body'),
    photos = document.querySelector('.photos'),
    slider = document.querySelector('.slider'),
    sliderBG = document.querySelector('.slider-bg');

  let startAt = null;

  photos.addEventListener('click', (e) => {
    slider.classList.add('active');
    body.classList.add('active');

    startAt = +e.target.dataset.slideNumber;
    console.log(startAt)

    new Glide('.slider', {
      autoplay: false,
      peek: 0,
      animationDuration: 1000,
      type: 'slider',
      animationTimingFunc: 'ease',
      startAt: startAt ? startAt - 1 : 0,
      focusAt: 0,
      perView: 1
    }).mount()
  });

  sliderBG.addEventListener('click', () => {
    slider.classList.remove('active');
    body.classList.remove('active');
  });
});